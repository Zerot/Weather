import { createUrl } from './index';

it('returns a valid url', () => {
  const result = createUrl('test', 'c');

  expect(result).toEqual('https://query.yahooapis.com/v1/public/yql?format=json&q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="test") and u="c"');
});

it('should throw an error when giving another unit than "c" or "f"', () => {
  expect(() => createUrl('test', 'd')).toThrow();
});

it('should throw an error when location is empty', () => {
  expect(() => createUrl('', 'c')).toThrow();
});
