// @flow
import { timeout } from '../util';

const requestTimeoutInMilliseconds = 5000;

export type Channel = {
  units: {
    distance: string,
    pressure: string,
    speed: string,
    temperature: string
  },
  location: {
    city: string,
    country: string,
    region: string
  },
  item: {
    link: string,
    condition: {
     code: string,
     temp: string,
     text: string
    },
    description: string,
    forecast: [{
      code: string,
      date: string, // "01 Sep 2018",
      day: string, // "Sat",
      high: string,
      low: string,
      text: string,
     }]
  }
}

export type ApiResponse = {
  query: {
    count: number,
    results: {
      channel: Channel
    }
  }
}

function handleResponse(res) {
  if (res.ok) {
    return res.json();
  }
  if (res.status === 400) {
    return Promise.reject(res.json().then(body => Error(body)));
  }

  return Promise.reject(Error(`Unknown error ${res.status}`));
}

function validateLocation(location: string): boolean {
  // We only check if a " is in the location because those will break the query.
  // This will still break if they encode the " or find another way to inject that character.
  // The yahoo yql (GET?) api apparently does not provide a way to escape a " within a string.
  // This means that we can only solve this in a blacklist manner
  return !location.includes('"');
}

function createUrl(location: string, temperatureUnit: string) {
  if (temperatureUnit !== 'c' && temperatureUnit !== 'f') {
    throw Error('invalid temperature unit. Can only be "c" or "f"');
  }

  if (!location) {
    throw Error('location is required');
  }

  const query = `select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="${location}") and u="${temperatureUnit}"`;
  const url = `https://query.yahooapis.com/v1/public/yql?format=json&q=${query}`;
  return url;
}

function getWeather({ location, temperatureUnit }: {location: string, temperatureUnit: "c" | "f"}): Promise<ApiResponse> {
  const url = createUrl(location, temperatureUnit);
  const resultPromise = timeout(fetch(url), requestTimeoutInMilliseconds)
    .then(handleResponse);
  return resultPromise;
}

export { getWeather, createUrl, validateLocation };
