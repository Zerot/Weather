// @flow
export function timeout<T>(promise: Promise<T>, ms: number): Promise<T> {
  const timerPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new Error('Timeout'));
    }, ms);
  });
  return Promise.race([timerPromise, promise]);
}
