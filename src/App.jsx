// @flow
import React, { Component, Fragment } from 'react';
import './App.css';
import { getWeather, validateLocation } from './yahooWeather';
import type { ApiResponse, Channel } from './yahooWeather';

type TemperatureUnit = "c" | "f"

function WeatherIcon({ code }: { code: string }) {
  return <i className={`wi wi-yahoo-${code}`} />;
}

function CurrentCondition({ channel }: { channel: Channel }) {
  if (!channel.location) {
    return (
      <div>Unable to determine location</div>
    );
  }

  return (
    <div className="currentCondition">
      <h1 className="title">
        The current weather in <em>{channel.location.city}, {channel.location.country}</em>
      </h1>
      <div className="row">
        <WeatherIcon code={channel.item.condition.code} />
        <div className="column">
          <div>{channel.item.condition.temp}&deg; {channel.units.temperature}</div>
          <div>{channel.item.condition.text}</div>
        </div>
      </div>
    </div>
  );
}

function Forecast({ channel }: { channel: Channel }) {
  if (!channel.location) {
    return (
      <div>Unable to determine location</div>
    );
  }

  return (
    <Fragment>
      <h1>Forecast:</h1>
      <div className="forecast">
        {channel.item.forecast.map(day => (
          <div className="day">
            <div className="title">{day.day} {day.date}</div>
            <div className="dayContent">
              <WeatherIcon code={day.code} />
              <div className="info">
                <div>High: {day.high}&deg; {channel.units.temperature}</div>
                <div>Low: {day.low}&deg; {channel.units.temperature}</div>
                <div>{day.text}</div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </Fragment>
  );
}

function Header() {
  return (
    <div className="header">
      Paidy Weather by Sander Homan
    </div>
  );
}

function TemperatureUnitSelector({ onChange }: { onChange: TemperatureUnit => void }) {
  return (
    <select onChange={e => onChange(e.target.value)}>
      <option value="c">Celcius</option>
      <option value="f">Fahrenheit</option>
    </select>
  );
}

function InputArea({ onLocationChange, onTemperatureUnitChange, onRefresh, error }) {
  return (
    <Fragment>
      <div className="inputArea">
        <input onChange={onLocationChange} placeholder="Location(e.g. Tokyo)" />
        <TemperatureUnitSelector onChange={onTemperatureUnitChange} />
        <button type="button" onClick={onRefresh}><i className="wi wi-refresh" /></button>
      </div>
      {error && <div className="error">{error}</div>}
    </Fragment>
  );
}

function Content({ data }) {
  if (!data) {
    return null;
  }

  if (data.query.count === 0 || !data.query.results.channel.location) {
    return <div>No results</div>;
  }

  return (
    <Fragment>
      <CurrentCondition channel={data.query.results.channel} />
      <Forecast channel={data.query.results.channel} />
    </Fragment>
  );
}

function Loading({ visible }) {
  return <div className="loading"><div className={`lds-dual-ring ${visible ? '' : 'hidden'}`} /></div>;
}

type State = {
  loading: boolean,
  error: string,
  data?: ApiResponse
}

class App extends Component<{}, State> {
  location: string

  temperatureUnit: TemperatureUnit

  updateWeatherTimeout: TimeoutID

  constructor() {
    super();
    this.state = { loading: true, error: '' };

    this.location = 'Tokyo';
    this.temperatureUnit = 'c';
    this.updateWeather();
  }

  updateWeather = () => {
    if (!this.location) { return; }

    if (this.updateWeatherTimeout) {
      clearTimeout(this.updateWeatherTimeout);
    }
    this.updateWeatherTimeout = setTimeout(() => {
      this.setState(
        { loading: true, error: '' },
        () => getWeather({ location: this.location, temperatureUnit: this.temperatureUnit })
          .then(data => this.setState({ loading: false, data }))
          .catch(() => this.setState({ loading: false, error: 'An error occured. Please try again' })),
      );
    }, 200);
  }

  inputChanged = (e: SyntheticInputEvent<>) => {
    if (!validateLocation(e.target.value)) {
      this.setState({ error: 'Invalid location' });
      return;
    }

    this.setState({ error: '' });
    this.location = e.target.value;
    this.updateWeather();
  }

  temperatureUnitChanged = (t: TemperatureUnit) => {
    this.temperatureUnit = t;
    this.updateWeather();
  }

  render() {
    const { loading, error, data } = this.state;
    return (
      <div className="container">
        <Header />
        <div className="content">
          <InputArea
            onLocationChange={this.inputChanged}
            onTemperatureUnitChange={this.temperatureUnitChanged}
            onRefresh={this.updateWeather}
            error={error}
          />
          <Loading visible={loading} />
          <Content data={data} />
        </div>
      </div>
    );
  }
}

export default App;
