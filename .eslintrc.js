module.exports = {
    "extends": ["airbnb"],
    "parser": "babel-eslint",
    "plugins": [ "jest" ],
    "rules": {
        "react/jsx-one-expression-per-line": "off",
        "react/sort-comp": "off",
        "react/prop-types": "off",
        "object-curly-newline": ["error", {
            "minProperties": 5
        }],
        "import/prefer-default-export": "off"
    },
    "env": {
        "browser": true,
        "jest/globals": true
    }
};